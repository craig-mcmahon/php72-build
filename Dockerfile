FROM php:7.2.23-cli
RUN apt-get update -yqq \
 && apt-get install -yqq \
        git \
        openssh-client \
        libmcrypt-dev \
        libpq-dev \
        libcurl4-gnutls-dev \
        libicu-dev \
        libvpx-dev \
        libjpeg-dev \
        libpng-dev \
        libxpm-dev \
        zlib1g-dev \
        libfreetype6-dev \
        libxml2-dev \
        libexpat1-dev \
        libbz2-dev \
        libgmp3-dev \
        libldap2-dev \
        unixodbc-dev \
        libsqlite3-dev \
        libaspell-dev \
        libsnmp-dev \
        libpcre3-dev \
        libtidy-dev \
        python \
        zip \
 && docker-php-ext-install \
        mbstring \
        pdo_pgsql \
        curl \
        json \
        intl \
        gd \
        xml \
        zip \
        bz2 \
        opcache \
 && pecl install xdebug \
 && docker-php-ext-enable xdebug \
 && curl -sS https://getcomposer.org/installer | php \
 && mv composer.phar /usr/local/bin/composer \
 && chmod +x /usr/local/bin/composer \
 && curl -sS https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-265.0.0-linux-x86_64.tar.gz > gcloud.tar.gz \
 && tar -xzf gcloud.tar.gz \
 && rm gcloud.tar.gz \
 && mv ./google-cloud-sdk /opt/google-cloud-sdk

ENV PATH="${PATH}:/opt/google-cloud-sdk/bin"